fuentes := $(shell find fonts -type f)
fuentes_instaladas := $(patsubst %,$(HOME)/.%,$(fuentes))

$(HOME)/.fonts/%: fonts/%
	cp -v $< $@

instalar-fuentes: $(fuentes_instaladas)
